"use strict";
/**
 * Compose a new function from a argument and another function.
 * @param n1
 * @param fn
 */
exports.compose = function (n2, fn) { return function (n1) { return fn(n1, n2); }; };
/**
 * Add two numbers
 * @param n1
 * @param n2
 */
exports.add = function (n1, n2) { return n1 + n2; };
/**
 * Subtract second number from first number
 * @param n1
 * @param n2
 */
exports.subtract = function (n1, n2) { return n1 - n2; };
/**
 * Add 1 to the given number
 * @type {(n2:number)=>number}
 */
exports.add1 = exports.compose(1, exports.add);
/**
 * Add 2 to the given number
 * @type {(n2:number)=>number}
 */
exports.add2 = exports.compose(2, exports.add);
/**
 * Subtract 1 from the given number
 * @type {(n2:number)=>number}
 */
exports.subtract1 = exports.compose(1, exports.subtract);
/**
 * Subtract 2 from the given number
 * @type {(n2:number)=>number}
 */
exports.subtract2 = exports.compose(2, exports.subtract);
//# sourceMappingURL=index.js.map