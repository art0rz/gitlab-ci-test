/**
 * Compose a new function from a argument and another function.
 * @param n1
 * @param fn
 */
export declare const compose: (n2: number, fn: (n1: number, n2: number) => number) => (n1: number) => number;
/**
 * Add two numbers
 * @param n1
 * @param n2
 */
export declare const add: (n1: number, n2: number) => number;
/**
 * Subtract second number from first number
 * @param n1
 * @param n2
 */
export declare const subtract: (n1: number, n2: number) => number;
/**
 * Add 1 to the given number
 * @type {(n2:number)=>number}
 */
export declare const add1: (n1: number) => number;
/**
 * Add 2 to the given number
 * @type {(n2:number)=>number}
 */
export declare const add2: (n1: number) => number;
/**
 * Subtract 1 from the given number
 * @type {(n2:number)=>number}
 */
export declare const subtract1: (n1: number) => number;
/**
 * Subtract 2 from the given number
 * @type {(n2:number)=>number}
 */
export declare const subtract2: (n1: number) => number;
