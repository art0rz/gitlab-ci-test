gitlab-ci-test
=
[![build status](https://gitlab.com/art0rz/gitlab-ci-test/badges/master/build.svg)](https://gitlab.com/art0rz/gitlab-ci-test/commits/master)
[![coverage report](https://gitlab.com/art0rz/gitlab-ci-test/badges/master/coverage.svg)](https://gitlab.com/art0rz/gitlab-ci-test/commits/master)

This is a simple node module to test GitLab's CI features

Installation
==
    npm install
    npm run build

Testing
==
    npm run test
    npm run coverage