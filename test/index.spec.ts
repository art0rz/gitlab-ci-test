import {add, subtract, subtract1, subtract2} from "../dist/index";
import * as chai from "chai";

describe('add',
	() => it('should add the second number to the first number',
		() => chai.expect(add(1, 2)).to.equal(3)));

describe('subtract',
	() => it('should subtract the second number from the first number',
		() => chai.expect(subtract(1, 2)).to.equal(-1)));

describe('subtract1',
	() => it('should subtract 1 from the given number',
		() => chai.expect(subtract1(2)).to.equal(1)));

describe('subtract2',
	() => it('should subtract 2 from the given number',
		() => chai.expect(subtract2(2)).to.equal(0)));