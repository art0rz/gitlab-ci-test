/**
 * Compose a new function from a argument and another function.
 * @param n1
 * @param fn
 */
export const compose = (n2:number, fn:(n1:number, n2:number) => number) => (n1:number) => fn(n1, n2);

/**
 * Add two numbers
 * @param n1
 * @param n2
 */
export const add = (n1:number, n2:number) => n1 + n2;

/**
 * Subtract second number from first number
 * @param n1
 * @param n2
 */
export const subtract = (n1:number, n2:number) => n1 - n2;

/**
 * Add 1 to the given number
 * @type {(n2:number)=>number}
 */
export const add1 = compose(1, add);

/**
 * Add 2 to the given number
 * @type {(n2:number)=>number}
 */
export const add2 = compose(2, add);

/**
 * Subtract 1 from the given number
 * @type {(n2:number)=>number}
 */
export const subtract1 = compose(1, subtract);

/**
 * Subtract 2 from the given number
 * @type {(n2:number)=>number}
 */
export const subtract2 = compose(2, subtract);